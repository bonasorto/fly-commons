package ru.fly.commons.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: fil
 * Date: 17.11.14
 */
public class AuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    public AuthenticationEntryPoint(String loginFormUrl){
        super(loginFormUrl);
    }

    @Override
    protected String determineUrlToUseForThisRequest(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
        String url = super.determineUrlToUseForThisRequest(request, response, exception);
        String servletPath = request.getServletPath();
        String query = request.getQueryString();
        if(query == null || !query.contains("redirect=")) {
            query = "redirect=" + servletPath.substring(servletPath.lastIndexOf("/"))
                    + (query == null ? "" : ("&" + query));
        }
        return url+"?"+query;
    }

}
