package ru.fly.commons.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author fil
 */
public class LogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        String url = determineTargetUrl(request, response);
        String query = request.getQueryString();
        if(query != null && !query.isEmpty()) {
            url += "?" + query;
        }
        redirectStrategy.sendRedirect(request, response, url);
    }
}
