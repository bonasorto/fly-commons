package ru.fly.commons.security;

import net.spy.memcached.MemcachedClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author fil
 */
public class MemcachedSecurityContextRepository implements SecurityContextRepository {

    private static final int ONE_DAY = 60 * 60 * 24;

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @Inject
    private MemcachedClient memcachedClient;

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder httpRequestResponseHolder) {
        HttpServletRequest req = httpRequestResponseHolder.getRequest();
        HttpSession ses = req.getSession(false);
        SecurityContext cxt = null;
        if (ses == null) {
            // try catch by jSessionId
            String sid = getCookiesJSessionId(req);
            if (sid != null) {
                cxt = (SecurityContext) memcachedClient.get(sid);
            } else {
                log.debug("Old JSESSIONID: " + sid);
            }
            ses = req.getSession(true);
            if (cxt != null) {
                Authentication authentication = cxt.getAuthentication();
                if (authentication instanceof PersistAuthenticationToken) {
                    ((PersistAuthenticationToken) authentication).setPersisted(false);
                }
            }
        } else {
            cxt = (SecurityContext) memcachedClient.get(ses.getId());
        }
        log.debug("JSESSIONID: " + ses.getId());
        if (cxt == null) {
            log.debug("Creating new security context!");
            cxt = SecurityContextHolder.createEmptyContext();
        }
        return cxt;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest req, HttpServletResponse resp) {
        Authentication authentication = context == null ? null : context.getAuthentication();
        // persist one time
        if (authentication instanceof PersistAuthenticationToken) {
            if (((PersistAuthenticationToken) authentication).isPersisted()) {
                return;
            } else {
                ((PersistAuthenticationToken) authentication).setPersisted(true);
            }
        }
        memcachedClient.set(getSid(req), ONE_DAY, context);
    }

    @Override
    public boolean containsContext(HttpServletRequest req) {
        try {
            return memcachedClient.get(getSid(req)) != null;
        } catch (Exception e) {
            return false;
        }
    }

    // ------------ privates --------------

    private String getSid(HttpServletRequest req) {
        HttpSession ses = req.getSession(false);
        return ses == null ? getCookiesJSessionId(req) : ses.getId();
    }

    private String getCookiesJSessionId(HttpServletRequest req) {
        if (req != null && req.getCookies() != null) {
            for (Cookie c : req.getCookies()) {
                if ("jsessionid".equalsIgnoreCase(c.getName())) {
                    return c.getValue();
                }
            }
        }
        return null;
    }
}
