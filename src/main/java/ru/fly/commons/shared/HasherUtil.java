package ru.fly.commons.shared;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author fil
 */
public class HasherUtil {

    private static  final char[] HEX_CHARS = new char[]{
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String getMd5(String value) {
        if ((value == null) || (value.isEmpty())) {
            throw new FlyCommonException ("Hashing string cant be NULL!");
        }
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new FlyCommonException("Hasher cant found algorithm", e);
        }
        md.reset();
        md.update(value.getBytes());
        return toHex(md.digest());
    }

    public static String getMd5WithSalt(String value, String salt){
        if ((value == null) || (value.isEmpty())) {
            throw new FlyCommonException ("Hashing string cant be NULL!");
        }
        return getMd5(salt.substring(0, salt.length() / 2) + getMd5(value) + salt.substring(salt.length() / 2));
    }

    // ------------------- privates ---------------------

    private static String toHex(byte[] bytes) {
        char[] arr = new char[bytes.length * 2];
        int j = 0;
        for (byte aByte : bytes) {
            arr[j++] = HEX_CHARS[(aByte & 0xF0) >> 4];
            arr[j++] = HEX_CHARS[aByte & 0x0F];
        }
        return new String(arr);
    }

}
