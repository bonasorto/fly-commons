package ru.fly.commons.shared;

/**
 * User: fil
 * Date: 26.07.14
 */
public class QuietException extends FlyCommonException {

    public QuietException() {}

    public QuietException(String msg){
        super(msg);
    }

}
