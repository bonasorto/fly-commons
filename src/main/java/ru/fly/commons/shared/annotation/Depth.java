package ru.fly.commons.shared.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User: fil
 * Date: 05.12.14
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Depth {

    int value() default  3/*Dehibernator.DEFAULT_DEPTH*/;

}
