package ru.fly.commons.shared.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * используется в Dehibernator, помечает поля, для которых необходимо убить ленивые зависимости Hibernate
 * User: fil
 * Date: 17.05.15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Documented
public @interface LazyKill {
}
