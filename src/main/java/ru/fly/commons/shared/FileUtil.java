package ru.fly.commons.shared;

/**
 * @author fil
 */
public class FileUtil {

    public final static long ONE_SECOND = 1000;
    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long ONE_DAY = ONE_HOUR * 24;

    /**
     * 1 kB = 1000 bytes (si = TRUE)
     * 1 KiB = 1024 bytes (si = FALSE)
     * @param bytes -
     * @param si  - приближенное вычисление
     * @return -
     */
    public static String humanReadableBytes(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        double size = (bytes / Math.pow(unit, exp));
        size = Math.round(size * 100.0) / 100.0;
        return size + " " + pre+"B";
    }

    public static String humanReadableDuration(long duration) {
        StringBuilder res = new StringBuilder();

        long temp = duration / ONE_DAY;
        if (temp > 0) {
            duration -= temp * ONE_DAY;
            res.append(temp).append("d").append(duration >= 0 ? ", " : "");
        }

        temp = duration / ONE_HOUR;
        if (temp > 0) {
            duration -= temp * ONE_HOUR;
            res.append(temp).append("h").append(duration >= 0 ? ", " : "");
        }

        temp = duration / ONE_MINUTE;
        if (temp > 0) {
            duration -= temp * ONE_MINUTE;
            res.append(temp).append("mi").append(duration >= 0 ? ", " : "");
        }

        temp = duration / ONE_SECOND;
        if (temp > 0) {
            duration -= temp * ONE_SECOND;
            res.append(temp).append("s").append(duration >= 0 ? ", " : "");
        }

        if (duration > 0) {
            res.append(duration).append("ms");
        }
        return res.toString();
    }

}
