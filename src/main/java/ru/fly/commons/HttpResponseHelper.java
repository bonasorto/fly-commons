package ru.fly.commons;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author fil
 */
public class HttpResponseHelper {

    private static final Logger log = LoggerFactory.getLogger(HttpResponseHelper.class.getName());

    public static void setResponseDocx(HttpServletRequest req, HttpServletResponse resp, String fileName)
            throws UnsupportedEncodingException {
        setResponse(req, resp, fileName, "docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    }

    public static void setResponseXlsx(HttpServletRequest req, HttpServletResponse resp, String fileName)
            throws UnsupportedEncodingException {
        setResponse(req, resp, fileName, "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }

    public static void setResponseTxt(HttpServletRequest req, HttpServletResponse resp, String fileName)
            throws UnsupportedEncodingException {
        setResponse(req, resp, fileName, "txt", "text/plain");
    }

    public static void setResponsePdf(HttpServletRequest req, HttpServletResponse resp, String fileName)
            throws UnsupportedEncodingException {
        setResponse(req, resp, fileName, "pdf", "application/octet-stream");
    }

    public static void setResponseBinary(HttpServletRequest req, HttpServletResponse resp, String fileNameAndExt)
            throws UnsupportedEncodingException {
        setResponse(req, resp, fileNameAndExt, "application/octet-stream");
    }

    public static void setResponseHtml(HttpServletResponse resp) throws UnsupportedEncodingException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding(Charsets.UTF_8.name());
    }

    public static void responseError(HttpServletResponse resp, String msg) {
        if (resp == null)
            return;
        try {
            resp.setContentType("text/html");
            resp.setCharacterEncoding(Charsets.UTF_8.name());
            StringBuilder ret = new StringBuilder("<!DOCTYPE html><html>")
                    .append("<head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"></head>")
                    .append("<body><center><b>").append(msg).append("</b></center></body></html>");
            IOUtils.write(ret.toString(), resp.getOutputStream(), Charsets.UTF_8);
            resp.flushBuffer();
        } catch (IOException ignore) {
        } catch (Exception e) {
            log.error("", e);
        }
    }

    // ------------------- privates ---------------------

    private static void setResponse(HttpServletRequest req, HttpServletResponse resp, String fileName,
                                    String ext, String mime) throws UnsupportedEncodingException {
        setResponse(req, resp, fileName + "." + ext, mime);
    }

    private static void setResponse(HttpServletRequest req, HttpServletResponse resp, String fileNameAndExt,
                                    String mime) throws UnsupportedEncodingException {
        resp.setContentType(mime);
        if (BrowserDetect.atLeast(req, UserAgent.Gecko)) {
            resp.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" +
                    URLEncoder.encode(fileNameAndExt, "UTF8").replace("+", "%20") + ";");
        } else {
            resp.setHeader("Content-Disposition", "attachment; filename=" +
                    URLEncoder.encode(fileNameAndExt, "UTF8").replace("+", " ") + ";");
        }
    }

}
