package ru.fly.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author fil
 */
public class NamedLock {

    private final HashMap<String, Lock> locks = new HashMap<>();
    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    public void lock(String name) {
        getLock(name, true).lock();
    }

    public boolean tryLock(String name) {
        return getLock(name, true).tryLock();
    }

    public void unlock(String name) {
        Lock lock = getLock(name, false);
        if (lock == null) {
            log.warn("Try unlock undefined NamedLock: " + name);
        } else {
            lock.unlock();
        }
    }

    private synchronized Lock getLock(String name, boolean isCreate) {
        if (locks.containsKey(name)) {
            return locks.get(name);
        } else {
            Lock lock = null;
            if (isCreate) {
                lock = new ReentrantLock();
                locks.put(name, lock);
            }
            return lock;
        }
    }

}
