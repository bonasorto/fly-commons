package ru.fly.commons;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.server.rpc.RPC;
import com.google.gwt.user.server.rpc.RPCRequest;
import com.google.gwt.user.server.rpc.SerializationPolicy;
import com.google.gwt.user.server.rpc.SerializationPolicyLoader;
import com.google.gwt.user.server.rpc.impl.LegacySerializationPolicy;
import com.google.gwt.user.server.rpc.impl.ServerSerializationStreamWriter;
import org.gwtwidgets.server.spring.GWTRPCServiceExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import ru.fly.commons.dehibernator.Dehibernator;
import ru.fly.commons.shared.FlyCommonException;
import ru.fly.commons.shared.QuietException;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.ParseException;

/**
 * @author fil
 */
public abstract class GwtRpcServiceExporter extends GWTRPCServiceExporter {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    @Inject
    private ApplicationContext applicationContext;
    private Class<? extends Exception> exceptionClass;

    public GwtRpcServiceExporter(Class<? extends Exception> exceptionClass){
        this.exceptionClass = exceptionClass;
    }

    @Override
    protected String invokeMethodOnService(Object service, Method targetMethod, Object[] targetParameters,
                                           RPCRequest rpcRequest) throws Exception {
        for(Object parameter : targetParameters){
            applicationContext.getBean(Dehibernator.class).hibernate(parameter);
        }
        Object result = targetMethod.invoke(service, targetParameters);
        try {
            result = applicationContext.getBean(Dehibernator.class).dehibernate(result);
        }catch (Exception e){
            throw new FlyCommonException("Ошибка в dehibernator", e);
        }
        SerializationPolicy serializationPolicy = getSerializationPolicyProvider()
                .getSerializationPolicyForSuccess(rpcRequest, service, targetMethod, targetParameters, result);
        return RPC.encodeResponseForSuccess(rpcRequest.getMethod(), result, serializationPolicy, serializationFlags);
    }

    @Override
    protected SerializationPolicy doGetSerializationPolicy(
            HttpServletRequest request, String moduleBaseURL, String strongName) {
        SerializationPolicy policy = super.doGetSerializationPolicy(request, moduleBaseURL, strongName);
        if(policy == null) {
            return loadReversProxySerializationPolicy(this, request, moduleBaseURL, strongName);
        } else {
            return policy;
        }
    }

    @Override
    protected String encodeResponseForFailure(RPCRequest rpcRequest, Throwable e, Method targetMethod,
                                              Object[] targetParameters) throws SerializationException {
        if (e != null && (e instanceof QuietException ||
                (e.getCause() != null && e.getCause() instanceof QuietException))) {
            log.debug("Message for client side:", e);
        } else {
            log.error(extendErrorLogMessage("Service side exception:"+
                    (e == null ? "" : e.getMessage()), getThreadLocalRequest()), e);
        }

        final ServerSerializationStreamWriter stream = createSerializationStreamWriter();
        try {
            if(e instanceof QuietException){
                stream.serializeValue(e, QuietException.class);
            }else{
                stream.serializeValue(wrap(e), exceptionClass);
            }
        } catch (SerializationException e1) {
            throw new RuntimeException(e1);
        }
        return "//EX" + stream.toString();
    }

    protected abstract Exception wrap(final Throwable e);

    protected String extendErrorLogMessage(String msg, HttpServletRequest request){
        return msg;
    }

    // ------------ privates ------------

    protected ServerSerializationStreamWriter createSerializationStreamWriter() {
        final ServerSerializationStreamWriter stream =
                new ServerSerializationStreamWriter(LegacySerializationPolicy.getInstance());
        stream.prepareToWrite();
        return stream;
    }

    /**
     * для реверс-проксированных запросов политики необходимо искать по полному пути
     */
    private SerializationPolicy loadReversProxySerializationPolicy(
            HttpServlet servlet, HttpServletRequest request, String moduleBaseURL, String strongName) {
        SerializationPolicy serializationPolicy = null;
        String rootHome = getServletContext().getRealPath("/");
        //usually ModuleBaseUrl is like http://127.0.0.1:8080/com.app.Main/
        String[] urlParts = moduleBaseURL.split("/");
        // we need com.app.Main
        String moduleName = urlParts[urlParts.length-1];
        String policyFileName = SerializationPolicyLoader.getSerializationPolicyFileName(strongName);
        String fullPath = rootHome + "/" + moduleName + "/" + policyFileName;
        InputStream is;
        try{
            is = new FileInputStream(fullPath);
        } catch (FileNotFoundException e) {
            servlet.log("Serialization policy file not found "+fullPath);
            return null;
        }
        try {
            try {
                serializationPolicy = SerializationPolicyLoader.loadFromStream(is, null);
            } catch (ParseException e) {
                servlet.log("ERROR: Failed to parse the policy file '" + fullPath + "'", e);
            } catch (IOException e) {
                servlet.log("ERROR: Could not read the policy file '" + fullPath + "'", e);
            }
        } finally {
            try {
                is.close();
            } catch (IOException ignored) {}
        }
        return serializationPolicy;
    }

}
