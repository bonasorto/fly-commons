package ru.fly.commons.poi;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;

import java.awt.*;
import java.util.Date;

/**
 * @author fil
 */
public class XLSXUtils {

    public static <T> void setCell(XSSFCell cell, T val) {
        setCell(cell, val, null);
    }

    public static <T> void setCell(XSSFCell cell, T val, XSSFCellStyle style) {
        if (style != null) {
            cell.setCellStyle(style);
        }
        if (val != null) {
            if (val instanceof Number) {
                cell.setCellValue(((Number) val).doubleValue());
            } else if (val instanceof Date) {
                cell.setCellValue((Date) val);
            } else {
                cell.setCellValue(String.valueOf(val));
            }
        }
    }

    public static XSSFCellStyle setStyleBorder(XSSFCellStyle style, Color color) {
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(new XSSFColor(color));
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(new XSSFColor(color));
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(new XSSFColor(color));
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(new XSSFColor(color));
        return style;
    }

    public static XSSFCellStyle setStyleBackground(XSSFCellStyle style, Color color) {
        style.setFillBackgroundColor(new XSSFColor(color));
        style.setFillForegroundColor(new XSSFColor(color));
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        return style;
    }

    public static void fillCells(XSSFRow row, int from, int to, XSSFCellStyle style) {
        for (int i = from; i <= to; i++) {
            setCell(row.createCell(i), "", style);
        }
    }

}
