package ru.fly.commons.poi;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRElt;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTRst;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge.CONTINUE;
import static org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge.RESTART;

/**
 * @author fil
 */
public class DOCXUtils {

    /**
     * @param doc -
     * @param tkn -
     * @param str -
     * @deprecated - use {@link DOCXUtils::replace}
     */
    public static void replaceStandard(XWPFDocument doc, String tkn, String str) {
        replace(doc, tkn, str);
    }

    /**
     * replace standard ${TOKEN} to some string value
     *
     * @param doc - document
     * @param tkn - token (without ${})
     * @param str - string value
     */
    public static List<XWPFRun> replace(XWPFDocument doc, String tkn, String str) {
        return replaceCustom(doc, "${" + tkn + "}", str);
    }

    public static void replace(XSSFRichTextString richString, String token, Object value) {
        if (token == null || token.trim().isEmpty()) {
            return;
        }
        replaceCustom(richString, "${" + token + "}", value);
    }

    public static void replace(XSSFCell cell, String token, Object value) {
        if (token == null || token.trim().isEmpty()) {
            return;
        }
        replaceCustom(cell, "${" + token + "}", value);
    }

    public static void replaceCustom(XSSFRichTextString richString, String token, Object value) {
        if (token == null || token.trim().isEmpty()) {
            return;
        }
        CTRst ctr = richString.getCTRst();
        for (CTRElt el : ctr.getRArray()) {
            el.setT(replaceInString(el.getT(), token, value == null ? "" : String.valueOf(value)));
        }
    }

    public static void replaceCustom(XSSFCell cell, String token, Object value) {
        if (token == null || token.trim().isEmpty()) {
            return;
        }
        cell.setCellValue(replaceInString(cell.getStringCellValue(),
                token, value == null ? "" : String.valueOf(value)));
    }

    public static List<XWPFRun> replaceCustom(XWPFDocument doc, String tkn, String str) {
        if (str == null) {
            str = "";
        }
        List<XWPFParagraph> ps = doc.getParagraphs();
        List<XWPFRun> ret = new ArrayList<>(replaceInParagraphs(ps, tkn, str));
        List<XWPFTable> tables = doc.getTables();
        for (XWPFTable t : tables) {
            List<XWPFTableRow> rows = t.getRows();
            for (XWPFTableRow row : rows) {
                List<XWPFTableCell> cells = row.getTableCells();
                for (XWPFTableCell cell : cells) {
                    ret.addAll(replaceInParagraphs(cell.getParagraphs(), tkn, str));
                }
            }
        }
        return ret;
    }

    private static List<XWPFRun> replaceInParagraphs(List<XWPFParagraph> ps, String tkn, String str) {
        if (str == null) {
            str = "";
        }
        List<XWPFRun> ret = new ArrayList<>();
        for (XWPFParagraph p : ps) {
            String txt = p.getText();
            if (txt.contains(tkn)) {
                ret.addAll(replaceInParagraph(p, tkn, str));
            }
        }
        return ret;
    }

    /**
     * replace text in paragraph and return list of affected XWPFRun
     *
     * @param p   - paragraph
     * @param tkn - full token with ${}
     * @param str - string value
     * @return - list of Runs
     */
    private static List<XWPFRun> replaceInParagraph(XWPFParagraph p, String tkn, String str) {
        if (str == null) {
            str = "";
        }
        List<XWPFRun> ret = new ArrayList<>();
        List<XWPFRun> rs = p.getRuns();
        List<XWPFRun> delRuns = new ArrayList<>();
        String txt = "";
        for (XWPFRun r : rs) {
            String rText = r.getText(0);
            if (rText == null)
                rText = "";
            if (rText.contains("$") || rText.contains("{") || rText.contains("}") || !txt.isEmpty()) {
                delRuns.add(r);
                txt += rText;
                if (txt.trim().endsWith("}") || txt.contains(tkn)) {
                    if (txt.contains(tkn)) {
                        delRuns.get(0).setText(replaceInString(txt, tkn, str), 0);
                        ret.add(delRuns.get(0));
                        for (int i = 1; i < delRuns.size(); i++) {
                            delRuns.get(i).setText("", 0);
                        }
                    }
                    delRuns.clear();
                    txt = "";
                }
            }
        }
        return ret;
    }

    private static String replaceInString(String src, String token, String value) {
        Matcher matcher = Pattern.compile(token.replace("$", "\\$").replace("{", "\\{")
                .replace("}", "\\}"), Pattern.CASE_INSENSITIVE).matcher(src);
        return matcher.replaceAll(value);
    }

    public static void setCellText(XWPFTableCell cell, String str, boolean center, boolean bold) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph p = cell.getParagraphs().get(0);
        if (center)
            p.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = p.createRun();
        run.setBold(bold);
        run.setText(str);
    }

    public static void setCellText(XWPFTableCell cell, String str, int size) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph p = cell.getParagraphs().get(0);
        XWPFRun run = p.createRun();
        run.setFontSize(size);
        run.setText(str);
    }

    public static void setCellText(XWPFTableCell cell, String str, String fontFamily, int size) {
        cell.setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
        XWPFParagraph p = cell.getParagraphs().get(0);
        XWPFRun run = p.createRun();
        run.setFontFamily(fontFamily);
        run.setFontSize(size);
        run.setText(str);
    }

    public static void setBottomBorder(XWPFTableCell cell, String color, Long size) {
        CTTcPr cttcpr = cell.getCTTc().getTcPr();
        if (cttcpr == null)
            cttcpr = cell.getCTTc().addNewTcPr();
        CTTcBorders borders = cttcpr.getTcBorders();
        if (borders == null)
            borders = cttcpr.addNewTcBorders();
        if (borders.getBottom() == null) borders.addNewBottom();
        borders.getBottom().setVal(STBorder.SINGLE);
        borders.getBottom().setColor(color);
        borders.getBottom().setSz(BigInteger.valueOf(size));
    }

    /**
     * @param cell
     * @param color - в hex без #, пример ff00aa
     */
    public static void setBorders(XWPFTableCell cell, String color, Long size) {
        CTTcPr cttcpr = cell.getCTTc().getTcPr();
        if (cttcpr == null)
            cttcpr = cell.getCTTc().addNewTcPr();
        CTTcBorders borders = cttcpr.getTcBorders();
        if (borders == null)
            borders = cttcpr.addNewTcBorders();
        if (borders.getTop() == null) borders.addNewTop();
        borders.getTop().setVal(STBorder.SINGLE);
        borders.getTop().setColor(color);
        borders.getTop().setSz(BigInteger.valueOf(size));
        if (borders.getRight() == null) borders.addNewRight();
        borders.getRight().setVal(STBorder.SINGLE);
        borders.getRight().setColor(color);
        borders.getRight().setSz(BigInteger.valueOf(size));
        if (borders.getBottom() == null) borders.addNewBottom();
        borders.getBottom().setVal(STBorder.SINGLE);
        borders.getBottom().setColor(color);
        borders.getBottom().setSz(BigInteger.valueOf(size));
        if (borders.getLeft() == null) borders.addNewLeft();
        borders.getLeft().setVal(STBorder.SINGLE);
        borders.getLeft().setColor(color);
        borders.getLeft().setSz(BigInteger.valueOf(size));
    }

    public static void setVMerge(XWPFTableCell cell, boolean restart) {
        CTTcPr cttcpr = cell.getCTTc().getTcPr();
        if (cttcpr == null) {
            cttcpr = cell.getCTTc().addNewTcPr();
        }
        CTVMerge vmerge = cttcpr.getVMerge();
        if (vmerge == null) {
            vmerge = cttcpr.addNewVMerge();
        }
        if (restart) {
            vmerge.setVal(RESTART);
        }
    }

    public static void mergeCells(XWPFTableRow row, int from, int to) {
        for (int i = from; i <= to; i++) {
            setHMerge(row.getCell(i), i == from);
        }
    }

    private static String getString(List<XWPFRun> runs) {
        if (runs == null) return null;
        if (runs.isEmpty()) return "";
        String ret = "";
        for (XWPFRun r : runs) {
            ret += r.getText(0);
        }
        return ret;
    }

    private static void setHMerge(XWPFTableCell cell, boolean restart) {
        CTTcPr cttcpr = cell.getCTTc().getTcPr();
        if (cttcpr == null) {
            cttcpr = cell.getCTTc().addNewTcPr();
        }
        CTHMerge hmerge = cttcpr.getHMerge();
        if (hmerge == null) {
            hmerge = cttcpr.addNewHMerge();
        }
        if (restart) {
            hmerge.setVal(RESTART);
        } else {
            hmerge.setVal(CONTINUE);
        }
    }

}
