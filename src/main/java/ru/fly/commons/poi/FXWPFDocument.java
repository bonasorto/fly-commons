package ru.fly.commons.poi;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author fil
 */
public class FXWPFDocument extends XWPFDocument{

    public FXWPFDocument() {
    }

    public FXWPFDocument(InputStream is) throws IOException {
        super(is);
    }

    public void replace(String tkn, String value) {
        DOCXUtils.replace(this, tkn, value);
    }

}
