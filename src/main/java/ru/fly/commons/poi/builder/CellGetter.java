package ru.fly.commons.poi.builder;

/**
 * @author fil.
 */
public interface CellGetter<O, V> {
    V get(O object);
}
