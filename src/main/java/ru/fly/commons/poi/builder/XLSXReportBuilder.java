package ru.fly.commons.poi.builder;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.fly.commons.HttpResponseHelper;
import ru.fly.commons.poi.XLSXUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.List;

/**
 * @author fil.
 */
public class XLSXReportBuilder<O> {

    private static final int DEFAULT_HEADER_HEIGHT = 1;
    private static final int DEFAULT_COLUMN_WIDTH = 10;

    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private XSSFWorkbook doc = new XSSFWorkbook();
    private String reportName;
    private int headerHeight = DEFAULT_HEADER_HEIGHT;
    private List<Integer> columnWidth;
    private List<String> columnHeaders;
    private List<CellGetter<O, ?>> cellGetters;
    private XSSFCellStyle hdrStyle;
    private XSSFCellStyle style;

    public XLSXReportBuilder() {
        XSSFFont font = doc.createFont();
        font.setFontHeight(10);
        font.setBold(true);
        font.setFontName("Calibri");
        hdrStyle = doc.createCellStyle();
        hdrStyle.setFont(font);
        hdrStyle.setWrapText(true);
        XLSXUtils.setStyleBackground(hdrStyle, new Color(221, 235, 247));
        XLSXUtils.setStyleBorder(hdrStyle, new Color(0, 0, 0));
        hdrStyle.setAlignment(HorizontalAlignment.CENTER);
        hdrStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        font = doc.createFont();
        font.setFontHeight(10);
        font.setFontName("Calibri");
        style = doc.createCellStyle();
        style.setFont(font);
        style.setWrapText(true);
        XLSXUtils.setStyleBorder(style, new Color(0, 0, 0));
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
    }

    public XLSXReportBuilder<O> reportName(String reportName) {
        this.reportName = reportName;
        return this;
    }

    public XLSXReportBuilder<O> columnWidth(List<Integer> columnWidth) {
        this.columnWidth = columnWidth;
        return this;
    }

    public XLSXReportBuilder<O> columnHeaders(List<String> columnHeaders) {
        this.columnHeaders = columnHeaders;
        return this;
    }

    public XLSXReportBuilder<O> cellGetters(List<CellGetter<O, ?>> cellGetters) {
        this.cellGetters = cellGetters;
        return this;
    }

    public XLSXReportBuilder<O> headerHeight(int height) {
        this.headerHeight = height;
        return this;
    }

    public XLSXReportBuilder<O> build(List<O> data) {
        int columnCount = Math.max(columnHeaders.size(), cellGetters.size());
        XSSFSheet sheet = doc.createSheet();
        for (int i = 0; i < columnCount; i++) {
            sheet.setColumnWidth(i, getColumnWidth(i) * 255);
        }
        int rowIdx = 0;
        XSSFRow row = sheet.createRow(rowIdx++);
        row.setHeightInPoints(Math.round(headerHeight * sheet.getDefaultRowHeightInPoints()));
        for (int i = 0; i < columnCount; i++) {
            XLSXUtils.setCell(row.createCell(i), getHeaderText(i), hdrStyle);
        }
        for (O d : data) {
            row = sheet.createRow(rowIdx++);
            for (int i = 0; i < columnCount; i++) {
                CellGetter<O, ?> getter = getCellGetter(i);
                XLSXUtils.setCell(row.createCell(i), getter == null ? null : getter.get(d), style);
            }
        }
        return this;
    }

    public XLSXReportBuilder<O> write(HttpServletRequest req, HttpServletResponse resp) {
        try {
            HttpResponseHelper.setResponseXlsx(req, resp, reportName);
            doc.write(resp.getOutputStream());
            resp.flushBuffer();
        } catch (IOException e) {
            log.error("Report generation failure, " + e.getMessage(), e);
            HttpResponseHelper.responseError(resp, "Ошибка формирования отчета!");
        }
        return this;
    }

    // ---------- privates -----------

    private Integer getColumnWidth(int idx) {
        return columnWidth.size() > idx ? columnWidth.get(idx) : DEFAULT_COLUMN_WIDTH;
    }

    private String getHeaderText(int idx) {
        return columnHeaders.size() > idx ? columnHeaders.get(idx) : "";
    }

    private CellGetter<O, ?> getCellGetter(int idx) {
        return cellGetters.size() > idx ? cellGetters.get(idx) : null;
    }

}
