package ru.fly.commons.dehibernator;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.inject.Inject;

/**
 * @author fil
 */
public class HbmDehibernatorImpl extends AbstractDehibernator {

    @Inject
    private SessionFactory sessionFactory;

    @Override
    protected Session openSession() {
        return sessionFactory.openSession();
    }
}
