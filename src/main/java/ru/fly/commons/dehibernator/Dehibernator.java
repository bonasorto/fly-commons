package ru.fly.commons.dehibernator;

import java.lang.reflect.InvocationTargetException;

/**
 * @author fil.
 */
public interface Dehibernator {

    <T> T hibernate(T object) throws InvocationTargetException, IllegalAccessException;

    <T> T dehibernate(T object);

}
