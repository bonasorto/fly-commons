package ru.fly.commons.dehibernator;

/*
    Спасибо автору Andrey Minogin, код для этого пакета честно взять по лицензии Apache License 2.0
    https://code.google.com/p/dehibernator/
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.fly.commons.shared.annotation.LazyKill;
import ru.fly.commons.shared.annotation.ServerOnly;
import ru.fly.commons.shared.annotation.Unchangeable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fil 
 */
public class ReflectionUtils {

    private static final Logger log = LoggerFactory.getLogger(ReflectionUtils.class.getName());

    public static List<String> getProperties(Object object) {
        List<String> properties = new ArrayList<>();
        Class<?> xClass = object.getClass();
        Method[] methods = xClass.getMethods();
        for (Method method : methods) {
            if (isSetter(method)) {
                String property = method.getName().substring(3);
                property = StringUtil.lcFirst(property);
                properties.add(property);
            }
        }
        return properties;
    }

    public static Field getField(Class clazz, String prop) {
        if(clazz == null || clazz == Object.class || prop == null){
            return null;
        }
        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        for(Field f : fields){
            if(prop.equals(f.getName())){
                return f;
            }
        }
        return getField(clazz.getSuperclass(), prop);
    }

    private static boolean isSetter(Method method){
        return method.getName().startsWith("set")
                && method.getParameterCount() == 1
                && "void".equals(method.getReturnType().getName());
    }

//    @SuppressWarnings("RedundantIfStatement")
//    private static boolean isGetter(Method method) {
//        String name = method.getName();
//        if (!name.startsWith("get"))
//            return false;
//        if (name.length() == 3)
//            return false;
//        if (name.equals("getClass"))
//            return false;
//        if (void.class.equals(method.getReturnType()))
//            return false;
//        if (method.getParameterTypes().length != 0)
//            return false;
//        return true;
//    }

    @SuppressWarnings("unchecked")
    public static <T> T get(Object object, String property) throws InvocationTargetException, IllegalAccessException {
        Class<?> xClass = object.getClass();
        Method method = getMethod(xClass,"get" + StringUtil.ucFirst(property));
        if(method == null){
            method = getMethod(xClass,"is" + StringUtil.ucFirst(property));
        }
        if(method == null){
            throw new IllegalStateException("Cant found getter for property: "+property+" of "+xClass.getName());
        }
        return (T) method.invoke(object);
    }

    public static void setIfPossible(Object object, String property, Object value)
            throws InvocationTargetException, IllegalAccessException {
        Class<?> xClass = object.getClass();
        Method method = getMethod(xClass, "set" + StringUtil.ucFirst(property));
        if (method == null)
            return;
        if(value == null && method.getParameterTypes()[0].isPrimitive()){
            return;
        }
        try {
            method.invoke(object, value);
        }catch (Exception e){
            log.error("Cant set property {} and value {} for {} check primitive state!", property, value, object);
        }
    }

    private static Method getMethod(Class<?> xClass, String name) {
        Method[] methods = xClass.getMethods();
        for (Method method : methods) {
            if (method.getName().equals(name))
                return method;
        }
        return null;
    }

    public static boolean hasUnchangeableAnnotation(Object obj, String prop) {
        return hasAnnotation(obj, prop, Unchangeable.class);
    }

    public static boolean hasServerOnlyAnnotation(Object obj, String prop) {
        return hasAnnotation(obj, prop, ServerOnly.class);
    }

    public static boolean hasLazyKillAnnotation(Object obj, String prop) {
        return hasAnnotation(obj, prop, LazyKill.class);
    }

    public static boolean hasAnnotation(Object obj, String prop, Class annotationClass) {
        if(obj == null){
            return false;
        }
        Field field = getField(obj.getClass(), prop);
        return field != null && field.getAnnotation(annotationClass) != null;
    }

}
