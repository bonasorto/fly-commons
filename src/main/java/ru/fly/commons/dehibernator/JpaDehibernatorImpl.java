package ru.fly.commons.dehibernator;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;

/**
 * @author fil
 */
public class JpaDehibernatorImpl extends AbstractDehibernator {

    @Inject
    private EntityManagerFactory entityManagerFactory;
    private SessionFactory sessionFactory;

    @PostConstruct
    public void init() {
        sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
    }

    @Override
    protected Session openSession() {
        return sessionFactory.openSession();
    }
}
