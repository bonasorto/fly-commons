/*
    Спасибо автору Andrey Minogin, код для этого класс честно взят и модифицирован по лицензии Apache License 2.0
    https://code.google.com/p/dehibernator/
 */
package ru.fly.commons.dehibernator;

import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.collection.internal.PersistentBag;
import org.hibernate.collection.internal.PersistentList;
import org.hibernate.collection.internal.PersistentMap;
import org.hibernate.collection.internal.PersistentSet;
import org.hibernate.collection.internal.PersistentSortedSet;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.internal.SessionImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ClassUtils;
import ru.fly.commons.shared.FlyCommonException;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author fil.
 */
public abstract class AbstractDehibernator implements Dehibernator {

    //    public static final int DEFAULT_DEPTH = 3;
    private final Logger log = LoggerFactory.getLogger(getClass().getName());

    private final Map<Object, Object> processed = new IdentityHashMap<>();
    private Map<Object, Object> etalons = new IdentityHashMap<>();

    private Session ses;
//    private int maxDepth = DEFAULT_DEPTH;
    /**
     * имена классов, для обектов, котрые не нужно обрабатывать, отдать как есть
     */
    private List<String> skipClassNames = new ArrayList<>();

    protected abstract Session openSession();

    public void setSkipClassNames(List<String> skipClassNames) {
        this.skipClassNames = skipClassNames;
    }

    public List<String> getSkipClassNames() {
        return skipClassNames;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T hibernate(T object) throws InvocationTargetException, IllegalAccessException {
        try {
            ses = openSession();
            return (T) doHibernate(object);
        } finally {
            ses.close();
        }
    }

    @Override
    public <T> T dehibernate(T object) {
        return dehibernate(object, 0);
    }

    @SuppressWarnings("unchecked")
    public <T> T dehibernate(T object, Integer depth) {
        try {
//            if(depth != null){
//                maxDepth = depth;
//            }
            ses = openSession();
            return (T) doDehibernate(object, 0, false);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            throw new FlyCommonException("Unexpected exception", e);
        } finally {
            try {
                ses.close();
            } catch (Exception e) {
                log.error("Session close exception!", e);
            }
        }
    }

    // -------------------- privates -----------------------

    private Object doHibernate(Object dirty) throws InvocationTargetException, IllegalAccessException {
        if (dirty == null) {
            return null;
        }

        if (processed.containsKey(dirty)) {
            return processed.get(dirty);
        }

        if (isPrimitive(dirty)) {
            return dirty;
        }

        processed.put(dirty, dirty);

        if (dirty instanceof Iterable) {
            for (Object o : (Iterable) dirty) {
                doHibernate(o);
            }
            return dirty;
        }

        for (String property : ReflectionUtils.getProperties(dirty)) {
            // если на поле висит аннотация ServerOnly, такое поле нужно подгрузить,
            // если модель изменяется, а не создается
            boolean etalonLoading = getIdentifierValue(dirty) != null &&
                    (ReflectionUtils.hasServerOnlyAnnotation(dirty, property) ||
                            ReflectionUtils.hasUnchangeableAnnotation(dirty, property));
            if (etalonLoading) {
                ReflectionUtils.setIfPossible(dirty, property, ReflectionUtils.get(getEtalon(dirty), property));
            } else {
                ReflectionUtils.setIfPossible(dirty, property, doHibernate(ReflectionUtils.get(dirty, property)));
            }
        }

        return dirty;
    }

    @SuppressWarnings("unchecked")
    private Object doDehibernate(Object dirty, int depth, boolean lazyKill)
            throws IllegalAccessException, InstantiationException, InvocationTargetException {
        try {
            if (dirty == null) {
                return null;
            }

            if (processed.containsKey(dirty)) {
                return processed.get(dirty);
            }

            if (isPrimitive(dirty) || skipClassNames.contains(dirty.getClass().getName())) {
                return dirty;
            }

            if (dirty instanceof PersistentBag) {
                PersistentBag dirtyList = (PersistentBag) dirty;
                List cleanList = new ArrayList<>();
                processed.put(dirtyList, cleanList);
                if (dirtyList.wasInitialized()) {
                    for (Object value : dirtyList) {
                        cleanList.add(doDehibernate(value, depth, lazyKill));
                    }
                }
                return cleanList;
            }

            if (dirty instanceof PersistentList) {
                PersistentList dirtyList = (PersistentList) dirty;
                List<Object> cleanList = new ArrayList<>();
                processed.put(dirtyList, cleanList);
                for (Object value : dirtyList) {
                    cleanList.add(doDehibernate(value, depth, lazyKill));
                }
                return cleanList;
            }

            if (dirty instanceof PersistentSortedSet) {
                PersistentSortedSet dirtySet = (PersistentSortedSet) dirty;
                Set<Object> cleanSet = new TreeSet<>();
                processed.put(dirtySet, cleanSet);
                for (Object value : dirtySet) {
                    cleanSet.add(doDehibernate(value, depth, lazyKill));
                }
                return cleanSet;
            }

            if (dirty instanceof PersistentSet) {
                PersistentSet dirtySet = (PersistentSet) dirty;
                Set<Object> cleanSet = new HashSet<>();
                processed.put(dirtySet, cleanSet);
                if (dirtySet.wasInitialized()/* || lazyKill*/) {
                    //                if(!dirtySet.wasInitialized()){
                    //                    dirtySet.setCurrentSession((SessionImplementor) ses);
                    //                    dirtySet.forceInitialization();
                    //                }
                    for (Object value : dirtySet) {
                        cleanSet.add(doDehibernate(value, depth, lazyKill));
                    }
                }
                return cleanSet;
            }

            if (dirty instanceof PersistentMap) {
                PersistentMap dirtyMap = (PersistentMap) dirty;
                Map<Object, Object> cleanMap = new LinkedHashMap<>();
                processed.put(dirtyMap, cleanMap);
                if (dirtyMap.wasInitialized()) {
                    for (Object key : dirtyMap.keySet()) {
                        Object value = dirtyMap.get(key);
                        cleanMap.put(doDehibernate(key, depth, lazyKill), doDehibernate(value, depth, lazyKill));
                    }
                }
                return cleanMap;
            }

            if (dirty instanceof List) {
                List<Object> dirtyList = (List<Object>) dirty;
                List<Object> cleanList = new ArrayList<>();
                processed.put(dirtyList, cleanList);
                for (Object aDirtyList : dirtyList) {
                    cleanList.add(doDehibernate(aDirtyList, depth, lazyKill));
                }
                return cleanList;
            }

            if (dirty instanceof LinkedHashMap) {
                Map<Object, Object> dirtyMap = (Map<Object, Object>) dirty;
                Map<Object, Object> cleanMap = new LinkedHashMap<>();
                processed.put(dirtyMap, cleanMap);
                for (Object key : dirtyMap.keySet()) {
                    Object value = dirtyMap.get(key);
                    cleanMap.put(doDehibernate(key, depth, lazyKill), doDehibernate(value, depth, lazyKill));
                }
                return cleanMap;
            }

            if (dirty instanceof HashMap) {
                Map<Object, Object> dirtyMap = (Map<Object, Object>) dirty;
                Map<Object, Object> cleanMap = new HashMap<>();
                processed.put(dirtyMap, cleanMap);
                for (Object key : dirtyMap.keySet()) {
                    Object value = dirtyMap.get(key);
                    cleanMap.put(doDehibernate(key, depth, lazyKill), doDehibernate(value, depth, lazyKill));
                }
                return cleanMap;
            }

            if (dirty instanceof LinkedHashSet<?>) {
                Set<Object> dirtySet = (LinkedHashSet<Object>) dirty;
                Set<Object> cleanSet = new LinkedHashSet<>();
                processed.put(dirtySet, cleanSet);
                for (Object value : dirtySet) {
                    cleanSet.add(doDehibernate(value, depth, lazyKill));
                }
                return cleanSet;
            }

            if (dirty instanceof HashSet<?>) {
                Set<Object> dirtySet = (HashSet<Object>) dirty;
                Set<Object> cleanSet = new HashSet<>();
                processed.put(dirtySet, cleanSet);
                for (Object value : dirtySet) {
                    cleanSet.add(doDehibernate(value, depth, lazyKill));
                }
                return cleanSet;
            }

            if (dirty instanceof TreeSet<?>) {
                Set<Object> dirtySet = (TreeSet<Object>) dirty;
                Set<Object> cleanSet = new TreeSet<>();
                processed.put(dirtySet, cleanSet);
                for (Object value : dirtySet) {
                    cleanSet.add(doDehibernate(value, depth, lazyKill));
                }
                return cleanSet;
            }

            if (dirty instanceof HibernateProxy) {
                dirty = ((HibernateProxy) dirty).getHibernateLazyInitializer();
            }

            if (dirty instanceof LazyInitializer) {
                LazyInitializer lazyInitializer = (LazyInitializer) dirty;
                if (lazyInitializer.isUninitialized()) {
                    if (lazyKill) {
                        dirty = ((SessionImpl) ses).immediateLoad(lazyInitializer.getPersistentClass().getName(),
                                lazyInitializer.getIdentifier());
                    } else {
                        processed.put(dirty, null);
                        return null;
                    }
                } else {
                    dirty = lazyInitializer.getImplementation();
                }
            }

            Object clear = dirty.getClass().newInstance();
            processed.put(dirty, clear);
            dehibernateProps(dirty, clear, depth);
            return clear;
        } catch (Exception e) {
            log.error("dirty = " + dirty.toString());
            throw e;
        }
    }

    private Serializable getIdentifierValue(Object object) throws InvocationTargetException, IllegalAccessException {
        ClassMetadata metadata = null;
        try {
            metadata = ses.getSessionFactory().getClassMetadata(object.getClass());
        } catch (MappingException ignored) {
        }
        if (metadata == null) {
            return null;
        }
        String idProp = metadata.getIdentifierPropertyName();
        if (idProp == null) {
            return null;
        }
        return ReflectionUtils.get(object, idProp);
    }

    /**
     * перезапросит объект из базы
     */
    private Object getEtalon(Object object) throws InvocationTargetException, IllegalAccessException {
        Object etalon = etalons.get(object);
        if (etalon == null) {
            etalon = ses.get(object.getClass(), getIdentifierValue(object));
            etalons.put(object, etalon);
        }
        return etalon;
    }

    private void dehibernateProps(Object dirty, Object clear, int depth) throws InvocationTargetException,
            IllegalAccessException, InstantiationException {
        for (String property : ReflectionUtils.getProperties(dirty)) {
            Object value = null;
            boolean lazyKill = ReflectionUtils.hasLazyKillAnnotation(dirty, property);
            //если на поле висит аннотация ServerOnly, такое поле на клиента отдавать не нужно
            if (!ReflectionUtils.hasServerOnlyAnnotation(dirty, property)) {
                value = ReflectionUtils.get(dirty, property);
                //если есть пометка убийсива ленивцев, получим их из эталона из базы
                if (value instanceof PersistentCollection &&
                        !((PersistentCollection) value).wasInitialized() && lazyKill) {
                    Object etalon = getEtalon(dirty);
                    if (etalon != null) {
                        value = ReflectionUtils.get(etalon, property);
                        ((PersistentCollection) value).forceInitialization();
                    }
                }
            }
            ReflectionUtils.setIfPossible(clear, property, doDehibernate(value, depth + 1, lazyKill));
        }
    }

    private boolean isPrimitive(Object object) {
        return ClassUtils.isPrimitiveOrWrapper(object.getClass()) ||
                ClassUtils.isPrimitiveArray(object.getClass()) ||
                ClassUtils.isPrimitiveWrapperArray(object.getClass()) ||
                object instanceof String ||
                object instanceof Enum ||
                object instanceof Date;

    }

}
