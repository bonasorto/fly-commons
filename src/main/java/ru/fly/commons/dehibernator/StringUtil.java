package ru.fly.commons.dehibernator;

/*
    Спасибо автору Andrey Minogin, код для этого пакета честно взять по лицензии Apache License 2.0
    https://code.google.com/p/dehibernator/
 */

/**
 * User: fil
 * Date: 26.09.14
 */
public class StringUtil {

    public static String ucFirst(String s) {
        if (s == null)
            return null;
        else if (s.isEmpty())
            return "";
        else
            return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String lcFirst(String s) {
        if (s == null)
            return null;
        else if (s.isEmpty())
            return "";
        else
            return s.substring(0, 1).toLowerCase() + s.substring(1);
    }

    public static boolean isEmpty(String s) {
        return s == null || s.trim().isEmpty();
    }

}
