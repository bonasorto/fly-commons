package ru.fly.commons.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fil.
 */
public final class JpaCriteriaBuilder<T> {

    private final EntityManager entityManager;
    private final Class<T> clazz;
    private final javax.persistence.criteria.CriteriaBuilder cb;
    private final CriteriaQuery query;
    private final Root<T> root;
    private Integer offset;
    private Integer limit;
    private List<Predicate> predicates = new ArrayList<>();
    private List<Predicate> orSequence = new ArrayList<>();
    private Map<String, From> joins = new HashMap<>();
    private List<Order> orders = new ArrayList<>();

    public JpaCriteriaBuilder(EntityManager entityManager, Class<T> clazz) {
        this.entityManager = entityManager;
        this.clazz = clazz;
        cb = entityManager.getCriteriaBuilder();
        query = cb.createQuery(clazz);
        root = query.from(clazz);
    }

    public JpaCriteriaBuilder<T> eq(String property, Object value) {
        finalizeOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        predicates.add(cb.equal(from.get(finalProp), value));
        return this;
    }

    public JpaCriteriaBuilder<T> orEq(String property, Object value) {
        prepareOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orSequence.add(cb.equal(from.get(finalProp), value));
        return this;
    }

    public JpaCriteriaBuilder<T> ne(String property, Object value) {
        finalizeOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        predicates.add(cb.notEqual(from.get(finalProp), value));
        return this;
    }

    public JpaCriteriaBuilder<T> orNe(String property, Object value) {
        prepareOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orSequence.add(cb.notEqual(from.get(finalProp), value));
        return this;
    }

    public JpaCriteriaBuilder<T> ilike(String property, String value) {
        finalizeOrSequence();
        if (value == null) {
            return this;
        }
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        predicates.add(cb.like(cb.upper(from.get(finalProp)), value.toUpperCase()));
        return this;
    }

    public JpaCriteriaBuilder<T> orIlike(String property, String value) {
        if (value == null) {
            return this;
        }
        prepareOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orSequence.add(cb.like(cb.upper(from.get(finalProp)), value.toUpperCase()));
        return this;
    }

    public <Y extends Comparable<Y>> JpaCriteriaBuilder<T> gt(String property, Y value) {
        finalizeOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        predicates.add(cb.greaterThan(from.get(finalProp), value));
        return this;
    }

    public <Y extends Comparable<Y>> JpaCriteriaBuilder<T> orGt(String property, Y value) {
        prepareOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orSequence.add(cb.greaterThan(from.get(finalProp), value));
        return this;
    }

    public <Y extends Comparable<Y>> JpaCriteriaBuilder<T> lt(String property, Y value) {
        finalizeOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        predicates.add(cb.lessThan(from.get(finalProp), value));
        return this;
    }

    public <Y extends Comparable<Y>> JpaCriteriaBuilder<T> orLt(String property, Y value) {
        prepareOrSequence();
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orSequence.add(cb.lessThan(from.get(finalProp), value));
        return this;
    }

    public JpaCriteriaBuilder<T> offset(int offset) {
        this.offset = offset;
        return this;
    }

    public JpaCriteriaBuilder<T> offset(long offset) {
        return offset((int) offset);
    }

    public JpaCriteriaBuilder<T> limit(int limit) {
        this.limit = limit;
        return this;
    }

    public JpaCriteriaBuilder<T> limit(long limit) {
        return limit((int) limit);
    }

    public JpaCriteriaBuilder<T> orderBy(String property) {
        return orderBy(property, true);
    }

    public JpaCriteriaBuilder<T> orderBy(String property, boolean asc) {
        From from = getFrom(property);
        String finalProp = property.substring(property.lastIndexOf(".") + 1);
        orders.add(asc ? cb.asc(from.get(finalProp)) : cb.desc(from.get(finalProp)));
        return this;
    }

    public Long countOf() {
        finalizeOrSequence();
        return (Long) entityManager.createQuery(query
                .select(cb.count(root)).where(cb.and(predicates.toArray(new Predicate[predicates.size()]))))
                .getSingleResult();
    }

    public List<T> list() {
        finalizeOrSequence();
        TypedQuery<T> q = entityManager.createQuery(query
                .select(root)
                .where(cb.and(predicates.toArray(new Predicate[predicates.size()])))
                .orderBy(orders));
        if (offset != null && offset > -1) {
            q.setFirstResult(offset);
        }
        if (limit != null && limit > -1) {
            q.setMaxResults(limit);
        }
        return q.getResultList();
    }

    // --------- privates -----------

    private void prepareOrSequence() {
        if (orSequence.size() == 0 && predicates.size() > 0) {
            Predicate p = predicates.get(predicates.size() - 1);
            orSequence.add(p);
            predicates.remove(p);
        }
    }

    private void finalizeOrSequence() {
        if (orSequence.size() > 0) {
            predicates.add(cb.or(orSequence.toArray(new Predicate[orSequence.size()])));
        }
        orSequence.clear();
    }

    private From getFrom(String property) {
        return getFrom(root, property);
    }

    private From getFrom(From from, String property) {
        if (!property.contains(".")) {
            return from;
        }
        int dotIdx = property.indexOf(".");
        String joinName = property.substring(0, dotIdx);
        From join = joins.computeIfAbsent(joinName, k -> from.join(joinName));
        return getFrom(join, property.substring(dotIdx + 1));
    }

}
