/*
 * Copyright 2015 Valeriy Filatov.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package ru.fly.commons.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hibernate.criterion.Restrictions.between;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.ge;
import static org.hibernate.criterion.Restrictions.in;
import static org.hibernate.criterion.Restrictions.isNotNull;
import static org.hibernate.criterion.Restrictions.isNull;
import static org.hibernate.criterion.Restrictions.le;
import static org.hibernate.criterion.Restrictions.like;
import static org.hibernate.criterion.Restrictions.ne;

/**
 * @author fil
 */
public class CriteriaBuilder {

    private final Criteria criteria;
    private final Set<String> aliases = new HashSet<>();
    private final Set<String> embeddedNames = new HashSet<>();

    public CriteriaBuilder(Session ses, Class clazz) {
        criteria = ses.createCriteria(clazz);
    }

    public void addEmbedded(String name) {
        embeddedNames.add(name);
    }

    public Criterion ilike(String property, Object value) {
        return Restrictions.ilike(processAliases(property), value);
    }

    public CriteriaBuilder addIsNull(String property) {
        criteria.add(isNull(processAliases(property)));
        return this;
    }

    public CriteriaBuilder addIsNotNull(String property) {
        criteria.add(isNotNull(processAliases(property)));
        return this;
    }

    public CriteriaBuilder addEq(String property, Object value) {
        criteria.add(eq(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addNe(String property, Object value) {
        criteria.add(ne(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addGe(String property, Object value) {
        criteria.add(ge(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addLe(String property, Object value) {
        criteria.add(le(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addIn(String property, Collection value) {
        criteria.add(in(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addLike(String property, Object value) {
        criteria.add(like(processAliases(property), value));
        return this;
    }

    public CriteriaBuilder addIlike(String property, Object value) {
        criteria.add(this.ilike(property, value));
        return this;
    }

    public CriteriaBuilder addBetween(String property, Object lo, Object hi) {
        criteria.add(between(processAliases(property), lo, hi));
        return this;
    }

    public CriteriaBuilder addOrder(String property, boolean asc) {
        if (property.endsWith(".fio")) {
            String prop = property.replace(".fio", ".lastName");
            criteria.addOrder(asc ? Order.asc(processAliases(prop)) : Order.desc(processAliases(prop)));
            prop = property.replace(".fio", ".firstName");
            criteria.addOrder(asc ? Order.asc(processAliases(prop)) : Order.desc(processAliases(prop)));
            prop = property.replace(".fio", ".middleName");
            criteria.addOrder(asc ? Order.asc(processAliases(prop)) : Order.desc(processAliases(prop)));
        } else {
            criteria.addOrder(asc ? Order.asc(processAliases(property)) : Order.desc(processAliases(property)));
        }
        return this;
    }

    public CriteriaBuilder offset(long offset) {
        criteria.setFirstResult((int) offset);
        return this;
    }

    public CriteriaBuilder limit(long limit) {
        criteria.setMaxResults((int) limit);
        return this;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> list() {
        return criteria.list();
    }

    // ------------ privates --------------

    private String processAliases(String path) {
        String[] aliases = path.split("\\.");
        if (aliases.length > 1) {
            String newPath = null;
            int endOffset = 1;//path.endsWith(".elements") ? 2 : 1;
            for (int i = 0; i < aliases.length - endOffset; i++) {
                if (newPath != null) {
                    newPath += ".";
                } else {
                    newPath = "";
                }
                String alias = aliases[i];
                newPath = newPath + alias;
                if (!embeddedNames.contains(alias)) {
                    newPath = checkAlias(newPath);
                }
            }
            for (int i = aliases.length - endOffset; i < aliases.length; i++) {
                newPath += ("." + aliases[i]);
            }
            return newPath;
        } else {
            return path;
        }
    }

    private String checkAlias(String path) {
        String alias = path.replace(".", "_") + "_";
        if (!aliases.contains(alias)) {
            criteria.createAlias(path, alias);
            aliases.add(alias);
        }
        return alias;
    }

}
